// // we assume this code runs at top level, inside a module
// let response = await fetch("https://jsonplaceholder.typicode.com/todos/1");
// let user = await response.json();

// console.log(user);

async function timeout(ms) {
  await new Promise((resolve) => setTimeout(resolve, ms));
  return 6666;
}

async function asyncFunc() {
  // const data1 = function cb(data) {
  //   return data;
  // };
  // setTimeout(() => {
  //   cb("success");
  // }, 2000);
  const data1 = await timeout(2000);
  return data1;
  // const d = await fetch("https://jsonplaceholder.typicode.com/todos/1");
  // return d;
}
async function syncFUnc() {
  let user = await asyncFunc();
  console.log("user", user);
  console.log("m i executing at last");
}

syncFUnc();
