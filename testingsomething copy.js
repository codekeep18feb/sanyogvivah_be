//convert a promise to async and vice versa
// conclusion they are both already thenable & awaitable

async function firstCall() {
  console.log("first");

  return 7;
}

async function secondCall() {
  console.log("second");

  return 3;
}

async function fetchData() {
  let d1 = await firstCall();
  console.log("now this line");
  let d2 = await secondCall();
  return d1 + d2;
}

fetchData().then((res) => console.log("first", res));
