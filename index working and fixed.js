const express = require("express");
const jwt = require("jsonwebtoken");
// const { applyMiddleware } = require("graphql-middleware");

const mongoose = require("mongoose");
const app = express();
const PORT = 6969;
const { graphqlHTTP } = require("express-graphql");
const schema = require("./Schemas/index");
const cors = require("cors");
const UserScema = require("./Schemas/mongoose_schemas/UserScema");

app.use(cors());
app.use(express.json());

mongoose.connect(process.env.DB_URI);
// , () => {
//   console.log("db connected now!!!!");
// });
async function authMidddleware(req, res, next) {
  const { authorization } = req.headers;
  if (!authorization) {
    // throw new Error("Unauthorized!");
    return res.status(403).json({ message: "Unauthorized!" });
  }
  let token = authorization.split(" ")[1];

  const email = await jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
  const u = await UserScema.findOne({ email: email });
  console.log("what the hack is this", u);
  req.user = u;

  next();
}
// app.use(authMidddleware);
app.use(
  "/graphql",
  // authenticateToken,
  authMidddleware,

  graphqlHTTP((req) => {
    console.log("reqdfdafdaf", req.user);
    return {
      schema,
      graphiql: true,
      context: { somekey: req.user },
    };
  })
);

async function fetchDataDummyApi() {
  console.log("fetch DAA WAS called!");
  return { from: "async" };
}

async function createUser(data) {
  try {
    const u1 = new UserScema({
      email: data.email,
      password: data.password,
      name: data.name,
      phone: data.phone,
    });
    data = await u1.save();
    // console.log("newly created user", u1);
    // const users1 = await UserScema.find();
    console.log("users1", data);

    return { data: data };
  } catch (e) {
    console.log("ehere is e", e.message);
    // throw Error(e.message);
    return { error: e };
  }
}

async function loginUser(data) {
  let { email, password } = data;
  try {
    //check if both matching
    //return the token
    console.log("password", password);
    const user_n = await UserScema.findOne(
      {
        password: password,
      },
      { email: email }
    );
    if (user_n) {
      const accessToken = jwt.sign(
        user_n.email,
        process.env.ACCESS_TOKEN_SECRET
      );
      return { data: { token: accessToken } };
    } else {
      return { error: "User Not Found!" };
    }
  } catch (e) {
    console.log("ehere is e", e.message);
    // throw Error(e.message);
    return { error: e };
  }
}

app.use("/register", async (req, res, next) => {
  const { email, password, name, phone } = req.body;
  // console.log(
  //   "here is all the details to register",
  //   email,
  //   password,
  //   name,
  //   phone
  // );
  // data = await fetchDataDummyApi();
  // console.log("do we see tis fetched data only once or ttwice", data);

  const { data, error } = await createUser(req.body);

  if (error) {
    res.status(401).json({ data: { message: error.message } });
  } else if (Object.keys(data).length > 0) {
    res.status(200).json({ data: data });
  }
});

app.use("/login", async (req, res, next) => {
  const { data, error } = await loginUser(req.body);

  if (error) {
    res.status(401).json({ data: { message: error.message } });
  } else if (Object.keys(data).length > 0) {
    res.status(200).json({ data: data });
  }
});

app.listen(PORT, () => {
  console.log("Server running");
});
