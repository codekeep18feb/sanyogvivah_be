// //this binding and it's possible effects
// //bind , apply,call

// const person1 = {
//   fname: "deepak",
//   lname: " singh",
//   fullName: function () {
//     return this.fname + this.lname;
//   },
// };

// // const person1 = {
// //   fname: "deepak",
// //   lname: " singh",
// //   fullName() {
// //     return this.fname + this.lname;
// //   },
// // };

// const employee1 = {
//   fname: "vijay",
//   lname: " singh",
// };
// //function borrowing by the other objects

// console.log(person1.fullName.apply(employee1));

//factory functions

// function Person(fname, lname) {
//   console.log(
//     "this should point to the function? no it points to global",
//     this
//   );
//   return {
//     fname,
//     lname,
//   };
// }

function Person(fname, lname) {

  console.log("this now", this);
  this.fname = fname;
  this.lname = lname;

}
const a = Person("myfname", "mylname");
console.log(a);
//constructor functions
//difference between regular function and arrow functions

//polymorphism , abstraction, incapsulation,inheritance.
//prototypal
