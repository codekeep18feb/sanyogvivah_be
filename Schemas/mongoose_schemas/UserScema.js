const mongoose = require("mongoose");
const ProfileSchema = require("./ProfileSchema");

const userScehma = new mongoose.Schema({
  email: { type: String, required: true, uppercase: true },
  password: { type: String, required: true },
  phone: { type: String, required: true },
  name: { type: String, required: true },
  createdAt: { type: Date, default: new Date() },
  updatedAt: { type: Date, required: true, default: new Date() },
});

userScehma.pre('save', function (next) {
  this.updatedAt = Date.now()
  next()
})




userScehma.post('save', async function (doc, next) {
  console.log('here create profile', doc._id)

  const p1 = new ProfileSchema({
    user_id: doc._id,
  });
  profile = await p1.save();
  console.log('saved profile', profile._id)
  next()
})

module.exports = mongoose.model("User", userScehma);
