const mongoose = require("mongoose");



const profileSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId, // here you set the author ID// from the Author colection, // so you can reference it
    required: true
  },
  name: { type: String }
});

module.exports = mongoose.model("profile", profileSchema);
