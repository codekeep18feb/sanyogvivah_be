require("dotenv").config();
const jwt = require("jsonwebtoken");

const graphql = require("graphql");
const {
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLInt,
	GraphQLString,
	GraphQLList,
} = graphql;
const userData = require("../MOCK_DATA.json");
const profileData = require("../MOCK_PROFILE_DATA.json");

const intrestsData = require("../MOCK_INTRESETS_DATA.json");

const AuthResponseType = require("./TypeDefs/AuthResponseType");

const { InterestType } = require("./TypeDefs/InterestsType");

const { ProfileFilterType, PaginationType, ProfileListType, ProfileDetailType } = require("./TypeDefs/ProfileType");
const UserType = require("./TypeDefs/UserType");

const RootQuery = new GraphQLObjectType({
	name: "RootQueryType",
	fields: {
		getAllUsers: {
			type: new GraphQLList(UserType),
			args: { id: { type: GraphQLInt } },
			resolve(parent, args) {
				return userData;
			},
		},

		getAllProfiles: {
			type: new GraphQLList(ProfileListType),
			args: { id: { type: GraphQLInt }, search: { type: GraphQLString }, filter: { type: ProfileFilterType }, pagination: { type: PaginationType } },
			resolve(parent, args, context) {
				console.log('we want to build  query here.', args)
				console.log(
					"are we able to get the secete ouf of context",
					context
				);
				return profileData;
			},
		},

		getAProfile: {
			type: ProfileDetailType,
			args: { id: { type: GraphQLInt } },
			resolve(parent, args) {
				id = args.id;
				console.log(
					"fetch data on the basis of this id",
					id,
					args
				);
				return profileData[0];
			},
		},

		getAllInterests: {
			type: new GraphQLList(InterestType),
			args: { id: { type: GraphQLInt }, search: { type: GraphQLString }, pagination: { type: PaginationType } },
			resolve(parent, args, context) {
				console.log('we want to build  query here.', args)
				console.log(
					"are we able to get the secete ouf of context",
					context
				);
				return intrestsData;
			},
		},
	},
});

const Mutation = new GraphQLObjectType({
	name: "Mutation",
	fields: {
		createUser: {
			type: UserType,
			args: {
				firstName: { type: GraphQLString },
				lastName: { type: GraphQLString },
				email: { type: GraphQLString },
				password: { type: GraphQLString },
			},
			resolve(parent, args) {
				userData.push({
					id: userData.length + 1,
					firstName: args.firstName,
					lastName: args.lastName,
					email: args.email,
					password: args.password,
				});
				return args;
			},
		},
		updateUser: {
			type: UserType,
			args: {
				id: { type: GraphQLInt },
				firstName: { type: GraphQLString },
				lastName: { type: GraphQLString },
				email: { type: GraphQLString },
				password: { type: GraphQLString },
			},
			resolve(parent, args) {
				userData.push({
					id: args.id,
					firstName: args.firstName,
					lastName: args.lastName,
					email: args.email,
					password: args.password,
				});
				return args;
			},
		},
	},
});

module.exports = new GraphQLSchema({ query: RootQuery, mutation: Mutation });
