const { GraphQLObjectType, GraphQLList, GraphQLInt, GraphQLString, GraphQLEnumType } = require("graphql");



const requestsStatusValues = {}
for (const value of ["waiting_for_approval", "approved", "rejected"]) {
        requestsStatusValues[value] = { value }
}

const RequestsStatusEnum = new GraphQLEnumType({
        name: 'RequestStatus',
        values: requestsStatusValues,
})


const ProfileListType = new GraphQLObjectType({
        name: "interestsType_profileList",
        fields: () => ({
                profile_id: { type: GraphQLInt },
                request_status: { type: RequestsStatusEnum }, //waiting_for_approval/approved/rejected

        }),
});

const InterestType = new GraphQLObjectType({
        name: "interestsType",
        fields: () => ({
                sent: { type: new GraphQLList(ProfileListType), }
        })
})

module.exports = { InterestType }