const graphql = require("graphql");
const { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList } = graphql;



// const ProfileFilterType = new GraphQLObjectType({
//   name: "ProfileFilterType",
//   fields: () => ({
//     stayin: { type: GraphQLString },

//   }),
// });


const ProfileDetailType = new GraphQLObjectType({
  name: "profileDetail",
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    familiydetails: { type: FamilyType },
  }),
});
// we had to update from objectType to InputObjectType //
const ProfileFilterType = new graphql.GraphQLInputObjectType({
  name: 'ProfileFilterType',
  fields: {
    stayin: { type: GraphQLString },

  }
})


const JobStatusType = new GraphQLObjectType({
  name: "JobStatusType",
  fields: () => ({ job_status: { type: GraphQLString } }),
});
const FamilyType = new GraphQLObjectType({
  name: "FamilyType",
  //NOIIT
  fields: () => ({
    stayin: { type: GraphQLString },
    nativeplace: { type: GraphQLString },
    // father: { job_status: { type: GraphQLString } },//this is not working
    father: { type: JobStatusType },
  }),
});

const ProfileListType = new GraphQLObjectType({
  name: "profile",
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    familiydetails: { type: FamilyType },
  }),
});

const PaginationType = new graphql.GraphQLInputObjectType({
  name: 'PaginationType',
  fields: {
    start: { type: GraphQLInt },
    end: { type: GraphQLInt },

  }
})


module.exports = { ProfileFilterType, PaginationType, ProfileDetailType, ProfileListType };
