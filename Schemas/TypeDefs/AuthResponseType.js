const graphql = require("graphql");
const { GraphQLObjectType, GraphQLInt, GraphQLString } = graphql;

const AuthResponseType = new GraphQLObjectType({
  name: "AuthResponse",
  fields: () => ({
    token: { type: GraphQLString },
  }),
});

module.exports = AuthResponseType;
